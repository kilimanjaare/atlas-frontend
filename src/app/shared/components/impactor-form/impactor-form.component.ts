import { Component, Input, OnInit } from '@angular/core';
import { Form } from '../../../admin/modules/plan-form/models/form.model';
import { ElementTypes } from '../../../admin/modules/plan-form/enums/element-types.enum';
import { FormElement } from '../../../admin/modules/plan-form/models/form-element.model';

@Component({
  selector: 'app-impactor-form',
  templateUrl: './impactor-form.component.html',
  styleUrls: ['./impactor-form.component.scss']
})
export class ImpactorFormComponent implements OnInit {

  @Input() formData: Form;

  elementTypes = ElementTypes;
  private value: { form_element_id: number, value?: any }[] = [];

  ngOnInit() {
    this.formData.elements.sort((element, nextElement) => element.weight - nextElement.weight);
  }

  submit() {
    // todo: validate all elements
    return this.value;
  }

  setValue(value: any, element: FormElement) {
    let existentElement = this.value.find(item => item.form_element_id === element.id);

    if (!existentElement) {
      existentElement = { form_element_id: element.id };
      this.value.push(existentElement);
    }

    switch (element.type) {
      case ElementTypes.checkbox: {
        if (existentElement.value) {
          if (value.checked) {
            existentElement.value.push(value.option);
          } else {
            existentElement.value = existentElement.value.filter(item => item !== value.option);
          }
        } else {
          existentElement.value = [value.option];
        }
        break;
      }
      case ElementTypes.grid: {
        if (existentElement.value) {
          if (value.checked) {
            existentElement.value.push({ row: value.row, column: value.column });
          } else {
            existentElement.value = existentElement.value.filter(item => !(item.row === value.row && item.column === value.column));
          }
        } else {
          existentElement.value = [{ row: value.row, column: value.column }];
        }
        break;
      }
      case ElementTypes.text:
      case ElementTypes.textarea:
      case ElementTypes.file:
      case ElementTypes.dropdown: {
        existentElement.value = value;
        break;
      }
    }
  }
}
