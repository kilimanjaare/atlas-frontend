import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UIModule } from './ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ImpactorFormComponent } from './components/impactor-form/impactor-form.component';
import { BaseLayoutComponent } from '@shared/layouts/base-layout/base-layout.component';

@NgModule({
  declarations: [
    ImpactorFormComponent,
    BaseLayoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    UIModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ImpactorFormComponent,
    UIModule,
    BaseLayoutComponent
  ]
})
export class SharedModule {
}
