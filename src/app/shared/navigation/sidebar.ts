export interface MenuItem {
  label: string;
  icon: string;
  link?: string;
  children?: MenuItem[];
  permission?: string[];
}

const menu: MenuItem[] = [
  {
    label: 'Plan',
    icon: 'fe-trending-up',
    link: '/admin/plans',
    children: [
      {
        link: '/admin/plans/new',
        label: 'Create plan',
        icon: 'fe-plus-circle'
      },
      {
        link: '/admin/plans/type/progress',
        label: 'Plan in progress',
        icon: 'fe-loader'
      },
      {
        link: '/admin/plans/type/completed',
        label: 'Completed plans',
        icon: 'fe-check-circle'
      }
    ]
  },
  {
    label: 'Requests',
    icon: 'fe-git-pull-request',
    link: '/admin/applications',
    children: [
      {
        link: '/admin/applications/type/in-progress',
        label: 'In progress',
        icon: 'fe-loader'
      },
      {
        link: '/admin/applications/type/favorites',
        label: 'Favorites',
        icon: 'fe-star'
      },
      {
        link: '/admin/applications/type/archived',
        label: 'Archived',
        icon: 'fe-archive'
      }
    ]
  },
  {
    label: 'Tasks',
    icon: 'fe-check-square',
    link: '/admin/tasks'
  },
  {
    label: 'Messages',
    icon: 'fe-mail',
    link: '/admin/dashboard'
  },
  {
    label: 'Update profile',
    icon: 'fe-settings',
    link: '/admin/dashboard'
  },
  {
    label: 'Help',
    icon: 'fe-help-circle',
    link: '/admin/dashboard'
  }
];

export default {
  menu
};
