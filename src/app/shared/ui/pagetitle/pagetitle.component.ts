import { Component, Input, TemplateRef } from '@angular/core';

export interface BreadcrumbItem {
  label: string;
  path?: string;
  active?: boolean;
}

@Component({
  selector: 'app-page-title',
  templateUrl: './pagetitle.component.html'
})
export class PagetitleComponent {
  @Input() breadcrumbItems: Array<{}>;
  @Input() title: string;
  @Input() leftContent: TemplateRef<any>;
}
