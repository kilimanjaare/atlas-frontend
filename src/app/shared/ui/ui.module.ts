import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  NgbAlertModule,
  NgbCollapseModule,
  NgbDatepickerModule,
  NgbDropdownModule,
  NgbPaginationModule,
  NgbTimepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';

import { SlimscrollDirective } from './slimscroll.directive';
import { PreloaderComponent } from './preloader/preloader.component';
import { PagetitleComponent } from './pagetitle/pagetitle.component';
import { RouterModule } from '@angular/router';
import { RatingModule } from 'ng-starrating';
import { FlatpickrModule } from 'angularx-flatpickr';

@NgModule({
  declarations: [
    SlimscrollDirective,
    PreloaderComponent,
    PagetitleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ClickOutsideModule,
    NgbCollapseModule,
    NgbTimepickerModule,
    RatingModule,
    FlatpickrModule.forRoot(),
    NgbDatepickerModule,
    NgbPaginationModule,
    NgbDropdownModule,
    NgbAlertModule
  ],
  exports: [
    SlimscrollDirective,
    PreloaderComponent,
    PagetitleComponent,
    ClickOutsideModule,
    NgbCollapseModule,
    NgbTimepickerModule,
    RatingModule,
    FlatpickrModule,
    NgbDatepickerModule,
    NgbPaginationModule,
    NgbAlertModule
  ]
})
export class UIModule {
}
