import { Component, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { SubSink } from 'subsink';
import { first } from 'rxjs/operators';
import { RoleApiService } from '@core/services/role-api.service';
import { AuthenticationService } from '@core/services/auth.service';
import { TranslationService } from '@core/services/translation.service';
import { UserApiService } from '@admin-modules/user/services/user-api.service';
import { UserService } from '@admin-modules/user/services/user.service';

@Component({
  selector: 'app-impactor',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnDestroy {
  public showModalBackground = false;
  private subSink: SubSink = new SubSink();

  constructor(
    private permissionsService: NgxPermissionsService,
    private roleApiService: RoleApiService,
    private translateService: TranslateService,
    private authService: AuthenticationService,
    private translationService: TranslationService,
    private userApiService: UserApiService,
    private userService: UserService,
  ) {
    this.loadTranslations();
    this.loadPermissions();
  }

  public ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  private loadTranslations(): void {
    this.translateService.setDefaultLang('en');

    this.subSink.sink = this.translationService.activeLang$.subscribe(lang => {
      this.translateService.use(lang);
    });
  }

  private loadPermissions(): void {
    if (this.authService.currentUser() && this.authService.currentUser().access_token) {
      this.roleApiService.getPermissions().pipe(first()).subscribe(permissions => {
        this.permissionsService.loadPermissions(permissions);
      });
    }
  }
}
