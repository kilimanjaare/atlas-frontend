import { Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import sidebar from '../../../shared/navigation/sidebar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnChanges {

  @Input() isCondensed = false;
  menu: any;
  items = sidebar.menu;

  @ViewChild('sideMenu', { static: false }) sideMenu: ElementRef;

  ngOnChanges() {
    if (!this.isCondensed && this.sideMenu) {
      setTimeout(() => {
      });
    } else if (this.menu) {
    }
  }
}
