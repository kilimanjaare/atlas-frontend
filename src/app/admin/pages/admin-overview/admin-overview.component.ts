import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin-overview.component.html'
})
export class AdminOverviewComponent implements AfterViewInit {

  isCondensed = false;

  ngAfterViewInit() {
    document.body.classList.remove('authentication-bg');
    document.body.classList.remove('authentication-bg-pattern');

    if (!this.isMobile()) {
      document.body.classList.add('sidebar-enable');
    }
  }

  displaySettings() {
    document.body.classList.toggle('right-bar-enabled');
  }

  toggleMobileMenu() {
    document.body.classList.toggle('sidebar-enable');

    if (!this.isMobile()) {
      document.body.classList.toggle('enlarged');
      this.isCondensed = !this.isCondensed;
    }
  }

  private isMobile() {
    const ua = navigator.userAgent;

    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua);
  }

}
