import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminOverviewComponent } from './pages/admin-overview/admin-overview.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { GuardService } from '@core/services/guard.service';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { BaseLayoutComponent } from '@shared/layouts/base-layout/base-layout.component';

const routes: Routes = [
  {
    path: '',
    component: AdminOverviewComponent,
    canActivate: [AuthGuard, GuardService, NgxPermissionsGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      { path: 'plans', loadChildren: () => import('./modules/plan/plan.module').then(m => m.PlanModule) },
      { path: 'tasks', loadChildren: () => import('./modules/task/task.module').then(m => m.TaskModule) },
      {
        path: 'applications',
        loadChildren: () => import('./modules/application/application.module').then(m => m.ApplicationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRouting {
}
