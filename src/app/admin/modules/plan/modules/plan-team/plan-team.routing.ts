import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PlanTeamListComponent } from '@admin-modules/plan/pages/plan-team-list/plan-team-list.component';
import { PlanTeamFormComponent } from '@admin-modules/plan/pages/plan-team-form/plan-team-form.component';

const routes: Routes = [
  {
    path: '',
    component: PlanTeamListComponent,
  },
  {
    path: 'add',
    component: PlanTeamFormComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanTeamRouting {
}
