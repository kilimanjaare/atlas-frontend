import { NgModule } from '@angular/core';
import { PlanTeamRouting } from '@admin-modules/plan/modules/plan-team/plan-team.routing';
import { PlanTeamListComponent } from '@admin-modules/plan/pages/plan-team-list/plan-team-list.component';
import { TeamModule } from '@admin-modules/team/team.module';
import { PlanTeamFormComponent } from '@admin-modules/plan/pages/plan-team-form/plan-team-form.component';

@NgModule({
  declarations: [
    PlanTeamListComponent,
    PlanTeamFormComponent,
  ],
  imports: [
    PlanTeamRouting,
    TeamModule,
  ]
})
export class PlanTeamModule {
}
