import { NgModule } from '@angular/core';
import { PlanApplicationRouting } from '@admin-modules/plan/modules/plan-application/plan-application.routing';
import { PlanApplicationListComponent } from '@admin-modules/plan/pages/plan-application-list/plan-application-list.component';
import { ApplicationModule } from '@admin-modules/application/application.module';
import { PlanApplicationFormComponent } from '@admin-modules/plan/pages/plan-application-form/plan-application-form.component';
import { PlanApplicationViewerComponent } from '@admin-modules/plan/pages/plan-application-viewer/plan-application-viewer.component';

@NgModule({
  declarations: [
    PlanApplicationListComponent,
    PlanApplicationFormComponent,
    PlanApplicationViewerComponent,
  ],
  imports: [
    PlanApplicationRouting,
    ApplicationModule,
  ]
})
export class PlanApplicationModule {
}
