import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PlanApplicationListComponent } from '@admin-modules/plan/pages/plan-application-list/plan-application-list.component';
import { PlanApplicationFormComponent } from '@admin-modules/plan/pages/plan-application-form/plan-application-form.component';
import { ActionRoutes } from '@core/enums/routes';
import { PlanApplicationViewerComponent } from '@admin-modules/plan/pages/plan-application-viewer/plan-application-viewer.component';

const routes: Routes = [
  {
    path: '',
    component: PlanApplicationListComponent,
  },
  {
    path: `:id/${ActionRoutes.edit}`,
    component: PlanApplicationFormComponent,
  },
  {
    path: `:id`,
    component: PlanApplicationViewerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanApplicationRouting {
}
