import { NgModule } from '@angular/core';
import { PlanRoutingModule } from './plan.routing';
import { PlanListComponent } from './pages/plan-list/plan-list.component';
import { PlanTableComponent } from './components/plan-table/plan-table.component';
import { PlanCreateModalComponent } from './components/plan-create-modal/plan-create-modal.component';
import { SharedModule } from '@shared/shared.module';
import { PlanViewComponent } from '@root/src/app/admin/modules/plan/pages/plan-view/plan-view.component';
import { PlanFormComponent } from '@root/src/app/admin/modules/plan/pages/plan-form/plan-form.component';
import { TranslateModule } from '@node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    PlanListComponent,
    PlanTableComponent,
    PlanCreateModalComponent,
    PlanViewComponent,
    PlanFormComponent
  ],
  imports: [
    SharedModule,
    PlanRoutingModule,
    TranslateModule
  ]
})
export class PlanModule {
}
