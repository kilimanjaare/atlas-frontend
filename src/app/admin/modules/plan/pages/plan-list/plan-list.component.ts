import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlanApiService } from '@core/services/plan.api.service';
import { Plan } from '@root/src/app/admin/modules/plan/models/plan.model';
import { MetaApi } from '@core/models/meta.api.model';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FoundationApiService } from '@admin-modules/foundation/services/foundation-api.service';
import { UserService } from '@admin-modules/user/services/user.service';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-plan-index',
  templateUrl: 'plan-list.component.html'
})
export class PlanListComponent {
  public plans: Plan[];
  public meta: MetaApi;
  public page = 1;
  public size = 10;
  public loading = false;
  public modalShow = false;
  public modalLoading = false;

  constructor(
    private modalService: NgbModal,
    private planApiService: PlanApiService,
    private foundationApiService: FoundationApiService,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private translateService: TranslateService,
  ) {
    this.loadPlans();
  }

  public createPlan(plan: any): void {
    this.modalLoading = true;

    this.planApiService.create(plan).pipe(first()).subscribe(response => {
      this.modalLoading = false;
      this.modalShow = false;

      this.router.navigate(['.', response.id], { relativeTo: this.route });
    });
  }

  public loadPlans(): void {
    this.loading = true;

    this.foundationApiService.getPlans(this.userService.current().foundation_id, {
      page: this.page,
      perPage: this.size
    }).subscribe(({ data, meta }) => {
      this.loading = false;
      this.plans = data;
      this.meta = meta;
    });
  }

  public destroy(id: number): void {
    if (confirm(this.translateService.instant('Do you really want to delete'))) {
      this.planApiService.destroy(id).pipe(first()).subscribe(() => this.loadPlans());
    }
  }

  public showApplications(id: number): void {
    this.router.navigate(['./', id, 'applications'], { relativeTo: this.route });
  }
}
