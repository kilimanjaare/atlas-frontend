import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PlanService } from '../../services/plan.service';
import { PlanApiService } from '@core/services/plan.api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { UserService } from '@admin-modules/user/services/user.service';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-component-plan-information',
  templateUrl: './plan-form.component.html'
})
export class PlanFormComponent implements OnInit, OnDestroy {
  public loading = true;
  public form: FormGroup;
  public isSubmitted = false;
  private subSink: SubSink = new SubSink();

  constructor(
    private fb: FormBuilder,
    private planService: PlanService,
    private planApiService: PlanApiService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private userService: UserService,
    private translateService: TranslateService,
  ) {
    this.createForm();
  }

  public ngOnInit(): void {
    this.subSink.sink = this.planService.planSubject$.subscribe(plan => {
      this.loading = false;
      this.form.patchValue(plan);
    });

    if (this.planService.plan) {
      this.loading = false;
      this.form.patchValue(this.planService.plan);
    }
  }

  public ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  public submit(): void {
    this.isSubmitted = true;

    if (this.form.valid) {
      this.loading = true;

      const id = this.form.value.id;
      let source = this.planApiService.create(this.form.value);

      if (id) {
        source = this.planApiService.update(id, this.form.value);
      }
      this.subSink.sink = source.pipe(
        finalize(() => this.loading = false)
      ).subscribe((response) => {
        this.planService.setPlan(response);
        this.toastr.success(this.translateService.instant('Plan was successfully updated')); // TODO: Add right color for success

        this.router.navigate(['../../', response.id], { relativeTo: this.route });
      });
    }
  }

  public showError(field: string): boolean {
    return this.form.get(field).invalid && this.isSubmitted;
  }

  private createForm(): void {
    this.form = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      description: [''],
      start_at: [''],
      expected_end_at: [''], // TODO: Add not less than start_at
      foundation_id: [this.userService.current().foundation_id],
    });

    if (this.planService.plan) {
      this.loading = false;
      this.form.patchValue(this.planService.plan);
    }
  }
}
