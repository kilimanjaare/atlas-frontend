import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreadcrumbItem } from '@shared/ui/pagetitle/pagetitle.component';
import { ActivatedRoute } from '@angular/router';
import { PlanApiService } from '@core/services/plan.api.service';
import { PlanService } from '../../services/plan.service';
import { Plan } from '@root/src/app/admin/modules/plan/models/plan.model';
import { SubSink } from '@node_modules/subsink';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-plan-view',
  templateUrl: './plan-view.component.html'
})
export class PlanViewComponent implements OnInit, OnDestroy {
  public breadcrumbItems: BreadcrumbItem[] = [
    { label: 'Dashboard', path: '/' },
    { label: 'Plans', path: '/plan' }
  ];
  public plan: Plan;
  public tabs = [];

  private subSink = new SubSink();

  constructor(
    private route: ActivatedRoute,
    private planApi: PlanApiService,
    private planService: PlanService,
    private translateService: TranslateService,
  ) {
    this.breadcrumbItems.push({
      label: `Plan unnamed ${route.snapshot.params.id}`,
      active: true
    });

    this.subSink.sink = this.planService.planSubject$.subscribe(plan => {
      this.plan = plan;
    });
  }

  public ngOnInit(): void {
    this.subSink.sink = this.route.paramMap.subscribe(params => {
      this.fetchData(params.get('id'));
    });
  }

  public ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  private fetchData(id: string): void {
    this.setTabs(id);

    if (id !== 'new') {
      this.subSink.sink = this.planApi.show(id, { include: 'currency' }).subscribe(response => {
        this.planService.setPlan(response);
      });
    } else {
      this.planService.setPlan({} as Plan);
    }
  }

  private setTabs(id: string): void {
    const defaultItem = { label: 'Information', key: 'information' };

    if (id === 'new') {
      this.tabs = [defaultItem];
    } else {
      this.tabs = [
        defaultItem,
        { label: this.translateService.instant('Budget'), key: 'budget' },
        { label: this.translateService.instant('Team'), key: 'team' },
        { label: this.translateService.instant('Forms'), key: 'forms' },
        { label: this.translateService.instant('Requests'), key: 'applications' },
        { label: this.translateService.instant('Projects'), key: 'projects' }
      ];
    }
  }
}
