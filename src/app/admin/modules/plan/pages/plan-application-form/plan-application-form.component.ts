import { Component, OnInit } from '@angular/core';
import { Application } from '@admin-modules/application/models/application.model';
import { ApplicationApiService } from '@admin-modules/application/services/application-api.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from '@node_modules/ngx-toastr';

@Component({
  selector: 'app-plan-application-form',
  templateUrl: 'plan-application-form.component.html'
})
export class PlanApplicationFormComponent implements OnInit {
  public application: Application;

  constructor(
    private applicationApiService: ApplicationApiService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
  ) {
  }

  public ngOnInit(): void {
    this.loadApplication();
  }

  public submit(data): void {
    this.applicationApiService.update(this.getId(), data).subscribe(response => {
      this.toastrService.success('Application was successfully updated');
    });
  }

  private loadApplication(): void {
    this.applicationApiService.show(this.getId(), { include: ['plan.currency'] }).subscribe(response => {
      this.application = response;
    });
  }

  private getId(): number {
    return this.route.snapshot.params.id;
  }
}
