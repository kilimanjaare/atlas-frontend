import { Component, OnInit } from '@angular/core';
import { ApplicationApiService } from '@admin-modules/application/services/application-api.service';
import { first } from '@node_modules/rxjs/internal/operators';
import { PlanApiService } from '@admin-modules/plan/services/plan-api.service';
import { Application } from '@admin-modules/application/models/application.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionRoutes } from '@core/enums/routes';
import { ToastrService } from '@node_modules/ngx-toastr';

@Component({
  selector: 'app-plan-application-list',
  templateUrl: 'plan-application-list.component.html'
})
export class PlanApplicationListComponent implements OnInit {
  public applications: Application[] = [];

  constructor(
    private applicationApiService: ApplicationApiService,
    private planApiService: PlanApiService,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
  ) {
  }

  public ngOnInit(): void {
    this.loadData();
  }

  public onEdit(id: number): void {
    this.router.navigate(['./', id, ActionRoutes.edit], { relativeTo: this.route });
  }

  public onView(id: number): void {
    this.router.navigate(['./', id], { relativeTo: this.route });
  }

  public onDestroy(id: number): void {
    if (confirm('Do you want to delete the application?')) {
      this.applicationApiService.destroy(id).pipe(first()).subscribe(response => {
        this.toastrService.success('Application was successfully deleted!');
        this.loadData();
      });
    }
  }

  private loadData(): void {
    this.planApiService.applications(this.getPlanId(), { include: ['organization', 'plan.currency'] })
      .pipe(first())
      .subscribe(response => {
        this.applications = response.data;
      });
  }

  private getPlanId(): number {
    return this.route.parent.parent.snapshot.params.id;
  }
}
