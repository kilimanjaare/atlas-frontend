import { Component, OnInit } from '@angular/core';
import { TableColumn } from '@core/models/table-column.model';
import { TeamApiService } from '@admin-modules/team/services/team-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanApiService } from '@admin-modules/plan/services/plan-api.service';
import { FoundationApiService } from '@admin-modules/foundation/services/foundation-api.service';
import { CookieService } from '@core/services/cookie.service';
import { User } from '@admin-modules/user/models/user.model';
import { TableAction } from '@core/models/table-action.model';
import { first } from '@node_modules/rxjs/internal/operators';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-plan-team-list',
  templateUrl: 'plan-team-list.component.html'
})
export class PlanTeamListComponent implements OnInit {
  public user: User;
  public teamPlanColumns: TableColumn[] = [
    {
      field: 'name',
      label: this.translateService.instant('Name'),
    },
    {
      field: 'phone',
      label: this.translateService.instant('Phone'),
    },
    {
      field: 'email',
      label: this.translateService.instant('Email'),
    }
  ];
  public teamPlanUsers: User[] = [];
  public teamPlanActions: TableAction[] = [
    {
      icon: 'fa fa-arrow-down',
      key: 'detach',
    }
  ];
  public teamFoundationColumns: TableColumn[] = [
    {
      field: 'name',
      label: this.translateService.instant('Name'),
    },
    {
      field: 'position',
      label: this.translateService.instant('Position'),
    },
    {
      field: 'phone',
      label: this.translateService.instant('Phone'),
    },
    {
      field: 'email',
      label: this.translateService.instant('Email'),
    }
  ];
  public teamFoundationUsers: User[] = [];
  public teamFoundationActions: TableAction[] = [
    {
      icon: 'fa fa-arrow-up',
      key: 'attach',
    }
  ];

  constructor(
    private teamApiService: TeamApiService,
    private planApiService: PlanApiService,
    private foundationApiService: FoundationApiService,
    private cookieService: CookieService,
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
  ) {
    this.loadUser();
  }

  public ngOnInit(): void {
    this.loadTeamPlan();
    this.loadTeamFoundation();
  }

  public attach({ id, key }): void {
    this.planApiService.attachUser(this.getPlanId(), id).pipe(first()).subscribe(response => {
      this.loadTeamPlan();
      this.loadTeamFoundation();
    });
  }

  public detach({ id, key }): void {
    this.planApiService.detachUser(this.getPlanId(), id).pipe(first()).subscribe(response => {
      this.loadTeamPlan();
      this.loadTeamFoundation();
    });
  }

  public add(): void {
    this.router.navigate(['./', 'add'], { relativeTo: this.route });
  }

  private loadTeamPlan(): void {
    this.planApiService.team(this.getPlanId()).subscribe(response => {
      this.teamPlanUsers = response;
    });
  }

  private loadTeamFoundation(): void {
    const filters = [
      { field: 'users.id', operator: '!=', value: this.user.id },
      { field: 'plans', operator: 'doesnt-have', value: this.getPlanId() }
    ];

    this.foundationApiService.team(this.user.foundation_id, { filters }).subscribe(response => {
      this.teamFoundationUsers = response;
    });
  }

  private loadUser(): void {
    this.user = JSON.parse(this.cookieService.getCookie('currentUser'));
  }

  private getPlanId(): number {
    return this.route.parent.parent.snapshot.params.id;
  }
}
