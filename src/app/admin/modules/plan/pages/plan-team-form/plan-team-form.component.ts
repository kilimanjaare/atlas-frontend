import { Component, OnInit } from '@angular/core';
import { User } from '@admin-modules/user/models/user.model';
import { ActivatedRoute } from '@angular/router';
import { UserApiService } from '@admin-modules/user/services/user-api.service';
import { first } from '@node_modules/rxjs/internal/operators';
import { ToastrService } from '@node_modules/ngx-toastr';
import { CookieService } from '@core/services/cookie.service';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-plan-team-form',
  templateUrl: 'plan-team-form.component.html'
})
export class PlanTeamFormComponent implements OnInit {
  public user: User;
  private authUser: User;

  constructor(
    private route: ActivatedRoute,
    private userApiService: UserApiService,
    private toastrService: ToastrService,
    private cookieService: CookieService,
    private translateService: TranslateService,
  ) {
    this.loadAuthUser();
  }

  public ngOnInit(): void {
    if (this.isEditable()) {
      this.loadUser();
    }
  }

  public isEditable(): boolean {
    return typeof this.route.snapshot.params.id !== 'undefined';
  }

  public submit(data: any): void {
    data.foundation_id = this.authUser.foundation_id;

    if (this.isEditable()) {
      this.userApiService.update(this.getPlanId(), data).subscribe(response => {
        this.toastrService.success(this.translateService.instant('User was successfully updated'));
      });
    } else {
      this.userApiService.create(data).subscribe(response => {
        this.toastrService.success(this.translateService.instant('User was successfully created'));
      });
    }
  }

  private loadUser(): void {
    this.userApiService.show(this.getPlanId(), { include: ['team'] })
      .pipe(first())
      .subscribe(response => {
        this.user = response;
      });
  }

  private loadAuthUser(): void {
    this.authUser = JSON.parse(this.cookieService.getCookie('currentUser'));
  }

  private getPlanId(): number {
    return this.route.snapshot.params.id;
  }
}
