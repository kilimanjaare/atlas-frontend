import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '@admin-modules/user/models/user.model';
import { CookieService } from '@core/services/cookie.service';

@Component({
  selector: 'app-plan-create-modal',
  templateUrl: './plan-create-modal.component.html'
})
export class PlanCreateModalComponent implements OnChanges {
  @ViewChild('content', { static: false }) public content: any;
  @Input() public show = false;
  @Input() public loading = false;
  @Output() public close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public submit: EventEmitter<any> = new EventEmitter<any>();

  public form: FormGroup;
  public submitted = false;
  public authUser: User;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private cookieService: CookieService,
  ) {
    this.getAuthUser();
    this.createForm();
  }

  public ngOnChanges({ show }: SimpleChanges): void {
    if (show && show.currentValue) {
      this.open(this.content);
    } else {
      this.modalService.dismissAll();
    }
  }

  public submitForm(): void {
    this.submitted = true;

    if (this.form.valid) {
      this.submit.emit(this.form.value);
    }
  }

  private open(content) {
    this.modalService.open(content).result.then(
      result => this.close.emit(true),
      reason => this.close.emit(true)
    );
  }

  private createForm(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      foundation_id: this.authUser.foundation_id,
    });
  }

  private getAuthUser(): void {
    this.authUser = JSON.parse(this.cookieService.getCookie('currentUser'));
  }
}
