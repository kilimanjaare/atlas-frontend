import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Plan } from '../../models/plan.model';

@Component({
  selector: 'app-plan-table',
  templateUrl: './plan-table.component.html',
})
export class PlanTableComponent {
  @Input() public plans: Plan[] = [];
  @Input() public total = 0;
  @Input() public page = 1;
  @Input() public size = 2;
  @Input() public loading = false;
  @Output() public pageChange: EventEmitter<number> = new EventEmitter();
  @Output() public sizeChange: EventEmitter<number> = new EventEmitter();
  @Output() public archive: EventEmitter<number> = new EventEmitter();
  @Output() public showApplications: EventEmitter<number> = new EventEmitter();
}
