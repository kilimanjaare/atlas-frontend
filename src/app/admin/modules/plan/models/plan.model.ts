import { Currency } from '@root/src/app/admin/modules/currency/models/currency.model';

export interface Plan {
  id?: number;
  name: string;
  description: string;
  budget: number;
  allocated_budget: number;
  currency_id: number;
  start_at: Date;
  currency?: Currency;
}
