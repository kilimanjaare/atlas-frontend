import { RouterModule, Routes } from '@angular/router';
import { PlanListComponent } from './pages/plan-list/plan-list.component';
import { NgModule } from '@angular/core';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { PlanViewComponent } from '@root/src/app/admin/modules/plan/pages/plan-view/plan-view.component';
import { PlanFormComponent } from '@root/src/app/admin/modules/plan/pages/plan-form/plan-form.component';

const routes: Routes = [
  {
    path: '',
    component: PlanListComponent,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: ['plan view'],
        redirectTo: '/'
      }
    }
  },
  {
    path: ':id',
    canActivate: [NgxPermissionsGuard],
    component: PlanViewComponent,
    data: {
      permissions: {
        only: ['plan view'],
        redirectTo: '/'
      }
    },
    children: [
      {
        path: '',
        redirectTo: 'information',
        pathMatch: 'full'
      },
      {
        path: 'information',
        component: PlanFormComponent
      },
      {
        path: 'budget',
        loadChildren: () => import('../budget/budget.module').then(m => m.BudgetModule)
      },
      {
        path: 'team',
        loadChildren: () => import('./modules/plan-team/plan-team.module').then(m => m.PlanTeamModule)
      },
      {
        path: 'forms',
        loadChildren: () => import('../plan-form/plan-form.module').then(m => m.PlanFormModule)
      },
      {
        path: 'applications',
        loadChildren: () => import('./modules/plan-application/plan-application.module').then(m => m.PlanApplicationModule)
        // loadChildren: () => import('../application/application.module').then(m => m.ApplicationModule)
      },
      {
        path: 'projects',
        loadChildren: () => import('../project/project.module').then(m => m.ProjectModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanRoutingModule {
}
