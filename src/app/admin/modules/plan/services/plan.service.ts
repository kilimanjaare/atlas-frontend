import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Plan } from '@root/src/app/admin/modules/plan/models/plan.model';

@Injectable({
  providedIn: 'root'
})
export class PlanService {
  public plan: Plan;
  public planSubject: Subject<Plan> = new Subject<Plan>();
  public planSubject$: Observable<Plan> = this.planSubject.asObservable();

  public setPlan(plan: Plan): void {
    this.plan = plan;
    this.planSubject.next(plan);
  }
}
