import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable } from '@node_modules/rxjs';
import { MetaParams } from '@core/models/api/metaParams';

@Injectable({
  providedIn: 'root'
})
export class PlanApiService extends ApiService {
  public baseUrl = 'plans';

  public applications(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/applications`, params);
  }

  public team(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/team`, params);
  }

  public attachUser(id: number, userId: number): Observable<any> {
    return this.post(`${id}/team`, { user_id: userId });
  }

  public detachUser(id: number, userId: number): Observable<any> {
    return this.delete(`${id}/team/${userId}`);
  }

}
