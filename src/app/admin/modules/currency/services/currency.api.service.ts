import { Injectable } from '@angular/core';
import { BaseApiService } from '@core/services/base.api.service';

@Injectable({
  providedIn: 'root',
})
export class CurrencyApiService extends BaseApiService {
  protected baseUrl = '/currencies';
}
