export interface Currency {
  id?: number;
  long_name: string;
  short_name: string;
}
