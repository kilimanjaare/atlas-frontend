import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { BudgetSettingsComponent } from './pages/budget-settings/budget-settings.component';
import { BudgetOverviewComponent } from './pages/budget-overview/budget-overview.component';

const routes: Routes = [
  {
    path: '',
    component: BudgetOverviewComponent,
  },
  {
    path: 'settings',
    component: BudgetSettingsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetRouting {
}
