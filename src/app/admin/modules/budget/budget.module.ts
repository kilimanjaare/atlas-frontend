import { NgModule } from '@angular/core';
import { BudgetRouting } from './budget.routing';
import { BudgetSettingsComponent } from './pages/budget-settings/budget-settings.component';
import { BudgetOverviewComponent } from './pages/budget-overview/budget-overview.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    BudgetSettingsComponent,
    BudgetOverviewComponent
  ],
  imports: [
    SharedModule,
    BudgetRouting,
    TranslateModule
  ]
})
export class BudgetModule {
}
