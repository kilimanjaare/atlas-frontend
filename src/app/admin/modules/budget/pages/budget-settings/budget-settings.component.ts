import { Component, OnDestroy, OnInit } from '@angular/core';
import { PlanService } from '../../../plan/services/plan.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PlanApiService } from '@core/services/plan.api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyApiService } from '../../../currency/services/currency.api.service';
import { List } from '@core/models/api/list';
import { ToastrService } from 'ngx-toastr';
import { SubSink } from '@node_modules/subsink';
import { Plan } from '@admin-modules/plan/models/plan.model';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-budget-settings',
  templateUrl: './budget-settings.component.html'
})
export class BudgetSettingsComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public plan: Plan;
  public currencies: List[] = [];

  private subSink = new SubSink();

  constructor(
    private planService: PlanService,
    private planApiService: PlanApiService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private currencyApi: CurrencyApiService,
    private toastr: ToastrService,
    private router: Router,
    private translateService: TranslateService,
  ) {
    this.createForm();
  }

  public ngOnInit(): void {
    if (this.planService.plan) {
      this.form.patchValue(this.planService.plan);
      this.plan = this.planService.plan;
    }

    this.subSink.add(
      this.planService.planSubject$.subscribe(plan => {
        this.form.patchValue(plan);
        this.plan = plan;
      }),
      this.currencyApi.list<List[]>().subscribe(response => this.currencies = response)
    );
  }

  public ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  public submit(): void {
    if (this.form.valid) {
      this.subSink.sink = this.planApiService.update(this.getId(), this.form.value).subscribe(response => {
        this.plan = response;
        this.planService.setPlan(response);
        this.router.navigate(['../'], { relativeTo: this.route });
        this.toastr.success(this.translateService.instant('Budget settings were successfully updated'));
      });
    }
  }

  public getCurrency() {
    const currency = this.currencies.find(item => +item.key === +this.plan.currency_id);

    return currency ? currency : {};
  }

  private createForm(): void {
    this.form = this.fb.group({
      budget: [null],
      currency_id: [null],
      total_projects: [null]
    });
  }

  private getId(): number {
    return this.route.parent.parent.snapshot.params.id;
  }
}
