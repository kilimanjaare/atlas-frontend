import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first, map } from 'rxjs/operators';
import { Application } from '@core/models/application.model';
import { PlanService } from '../../../plan/services/plan.service';
import { ChartService } from '@core/services/chart.service';
import { PlanApiService } from '@core/services/plan.api.service';
import { Plan } from '../../../plan/models/plan.model';
import { SubSink } from '@node_modules/subsink';

export interface XYChart {
  month: number;
  amount: number;
}

@Component({
  selector: 'app-budget-overview',
  templateUrl: './budget-overview.component.html',
  styleUrls: ['./budget-overview.component.scss']
})
export class BudgetOverviewComponent implements OnInit, OnDestroy {
  public plan: Plan;
  public allocations: Application[] = [];
  public dateType = 'monthly';

  private subSink = new SubSink();

  constructor(
    private planService: PlanService,
    private router: Router,
    private route: ActivatedRoute,
    private chartService: ChartService,
    private planApiService: PlanApiService
  ) {
  }

  public ngOnInit(): void {
    if (this.planService.plan) {
      this.initBudget(this.planService.plan);
    }

    this.subSink.sink = this.planService.planSubject$.subscribe(plan => {
      this.initBudget(plan);
    });
  }

  public ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  private initBudget(plan: Plan): void {
    if (!this.checkRedirect(plan)) {
      this.plan = plan;

      this.initBudgetAllocationChart();
      this.initBudgetSplineChart();
      this.loadAllocations();
    }
  }

  private checkRedirect(plan: Plan): boolean {
    if (!plan.budget) {
      this.router.navigate(['./', 'settings'], { relativeTo: this.route });
    }

    return !plan.budget;
  }

  private initBudgetAllocationChart(): void {
    const percentage = Math.round(this.plan.allocated_budget / this.plan.budget * 100);

    this.chartService.generatePercentageChart('#chart-left', { percentage, color: 'lightgreen' });
  }

  private initBudgetSplineChart(): void {
    this.subSink.sink = this.planApiService.budgetChart(this.plan.id, this.dateType).subscribe((response: XYChart[]) => {
      const data: any = [
        ['x'], ['line']
      ];

      response.forEach(item => {
        data[0].push(item.month);
        data[1].push(item.amount);
      });

      this.chartService.generateMonthChart('#chart-spline', data);
    });
  }

  private loadAllocations(): void {
    this.planApiService.applications(this.plan.id).pipe(first()).subscribe(response => {
      this.allocations = response.data;
    });
  }
}
