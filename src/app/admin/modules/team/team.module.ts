import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { TeamPlanComponent } from './components/team-plan/team-plan.component';
import { TeamOrganizationComponent } from './components/team-organization/team-organization.component';
import { TeamFormComponent } from './components/team-form/team-form.component';
import { TeamRouting } from '@admin-modules/team/team.routing';
import { TeamListComponent } from '@admin-modules/team/pages/team-list/team-list.component';
import { TeamTableComponent } from '@admin-modules/team/components/team-table/team-table.component';
import { TranslateModule } from '@node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    TeamPlanComponent,
    TeamOrganizationComponent,
    TeamFormComponent,
    TeamListComponent,
    TeamTableComponent,
  ],
  imports: [
    SharedModule,
    TeamRouting,
    TranslateModule,
  ],
  exports: [
    TeamPlanComponent,
    TeamOrganizationComponent,
    TeamFormComponent,
    TeamTableComponent,
  ]
})
export class TeamModule {
}
