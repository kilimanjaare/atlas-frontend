import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '@core/services/api.service';

@Injectable({
  providedIn: 'root',
})
export class TeamApiService extends ApiService {
  public baseUrl = 'teams';

  public organization(): Observable<any> {
    return this.get(`${this.baseUrl}/organization?include=user`);
  }

  public plan(id: number): Observable<any> {
    return this.get(`plan/${id}`);
  }

  public store(data: any): Observable<any> {
    return this.post(`${this.baseUrl}`, data);
  }

  public detachFromTeam(userId: number, planId: number): Observable<any> {
    return this.put(`${this.baseUrl}/detach-from-plan`, { user: userId, plan: planId });
  }
}
