import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TeamApiService } from '../../services/team-api.service';
import { Team } from '../../models/team.model';

@Component({
  selector: 'app-team-organization',
  templateUrl: './team-organization.component.html'
})
export class TeamOrganizationComponent {
  @Input() public heading = '';
  @Output() public addPerson: EventEmitter<boolean> = new EventEmitter<boolean>();

  public team: Team[] = [];

  constructor(
    private teamApiService: TeamApiService,
  ) {
    this.teamApiService.organization().subscribe(response => {
      this.team = response.data;
    });
  }
}
