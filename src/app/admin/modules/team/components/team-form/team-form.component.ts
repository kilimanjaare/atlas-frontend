import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../user/models/user.model';

@Component({
  selector: 'app-team-form',
  templateUrl: 'team-form.component.html'
})
export class TeamFormComponent implements OnInit {
  @Input() public user: User;
  @Input() public editable = false;
  @Output() public submitted: EventEmitter<any> = new EventEmitter();
  public form: FormGroup;
  public isSubmitted = false;

  constructor(
    private fb: FormBuilder,
  ) {
  }

  public ngOnInit(): void {
    this.createForm();
  }

  public submit(): void {
    this.isSubmitted = true;

    if (this.form.valid) {
      this.submitted.emit(this.form.value);
    }
  }

  public showError(field: string): boolean {
    return this.form.get(field).invalid && this.isSubmitted;
  }

  private createForm(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      position: '',
      email: ['', [Validators.email, Validators.required]],
      phone: '',
      role: '',
    });
  }
}
