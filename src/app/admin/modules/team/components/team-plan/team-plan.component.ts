import { Component, Input, OnInit } from '@angular/core';
import { TeamApiService } from '../../services/team-api.service';
import { Team } from '../../models/team.model';

@Component({
  selector: 'app-team-plan',
  templateUrl: './team-plan.component.html'
})
export class TeamPlanComponent implements OnInit {
  @Input() public heading = '';
  @Input() public planId: number;

  public team: Team[] = [];

  constructor(
    private teamApiService: TeamApiService,
  ) {
  }

  public ngOnInit(): void {
    this.teamApiService.plan(this.planId).subscribe(response => {
      this.team = response.data;
    });
  }
}
