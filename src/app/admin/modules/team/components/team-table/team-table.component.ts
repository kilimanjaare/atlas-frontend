import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TableColumn } from '@core/models/table-column.model';
import { User } from '@admin-modules/user/models/user.model';
import { TableAction } from '@core/models/table-action.model';

@Component({
  selector: 'app-team-table',
  templateUrl: 'team-table.component.html'
})
export class TeamTableComponent {
  @Input() public heading: string;
  @Input() public columns: TableColumn[] = [];
  @Input() public actions: TableAction[] = [];
  @Input() public users: User[] = [];
  @Input() public canAdd = false;
  @Output() public actionClick: EventEmitter<{ id: number, key: string }> = new EventEmitter();
  @Output() public addClick: EventEmitter<void> = new EventEmitter();
}
