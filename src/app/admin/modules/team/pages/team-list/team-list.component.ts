import { Component, OnInit } from '@angular/core';
import { Team } from '@admin-modules/team/models/team.model';
import { Plan } from '@admin-modules/plan/models/plan.model';
import { TeamApiService } from '@admin-modules/team/services/team-api.service';
import { PlanService } from '@admin-modules/plan/services/plan.service';
import { UserService } from '@admin-modules/user/services/user.service';
import { UserApiService } from '@admin-modules/user/services/user-api.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html'
})
export class TeamListComponent implements OnInit {
  public teamOrganization: Team[] = [];
  public teamPlan: Team[] = [];
  public plan: Plan;

  constructor(
    private teamApiService: TeamApiService,
    private planService: PlanService,
    private userService: UserService,
    private userApiService: UserApiService,
  ) {
  }

  public ngOnInit(): void {
    this.loadProject();
    this.loadOrganizationTeam();
  }

  public attachToPlan(id: number): void {
  }

  public detachFromPlan(id: number): void {
    this.teamApiService.detachFromTeam(id, this.plan.id).subscribe(response => {
      this.loadOrganizationTeam();
      this.loadPlanTeam();
    });
  }

  public destroy(id: number): void {
    if (confirm('Do you really want to delete member?')) {
      this.userApiService.destroy(id).subscribe(response => {
        this.loadOrganizationTeam();
        this.loadPlanTeam();
      });
    }
  }

  private loadOrganizationTeam(): void {
    this.teamApiService.organization().subscribe(response => {
      this.teamOrganization = response.data;
    });
  }

  private loadPlanTeam(): void {
    this.teamApiService.plan(this.plan.id).subscribe(response => {
      this.teamPlan = response.data;
    });
  }

  private loadProject(): void {
    if (this.planService.plan) {
      this.plan = this.planService.plan;
      this.loadPlanTeam();
    }

    this.planService.planSubject$.subscribe(plan => {
      this.plan = plan;
      this.loadPlanTeam();
    });
  }
}
