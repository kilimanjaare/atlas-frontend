import { User } from '../../user/models/user.model';

export interface Team {
  id: number;
  plan_id: number;
  user: User;
}
