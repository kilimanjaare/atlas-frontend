import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { TeamListComponent } from '@admin-modules/team/pages/team-list/team-list.component';

const routes: Routes = [
  {
    path: '',
    component: TeamListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRouting {
}
