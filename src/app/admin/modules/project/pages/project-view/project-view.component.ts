import { Component } from '@angular/core';

@Component({
  selector: 'app-plan-project-view',
  templateUrl: './project-view.component.html'
})
export class ProjectViewComponent {
  public tabs = [
    { label: 'Description', key: 'overview' },
    { label: 'Budget and Goals', key: 'plan-budget-goals' },
    { label: 'Timeline', key: 'timeline' },
  ];
}
