import { NgModule } from '@angular/core';
import { ProjectIndexComponent } from './pages/project-index/project-index.component';
import { ProjectViewComponent } from './pages/project-view/project-view.component';
import { ProjectOverviewComponent } from './pages/project-overview/project-overview.component';
import { ProjectMilestoneComponent } from './pages/project-milestone/project-milestone.component';
import { ProjectTimelineComponent } from './pages/project-timeline/project-timeline.component';
import { ProjectRouting } from './project.routing';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    ProjectIndexComponent,
    ProjectViewComponent,
    ProjectOverviewComponent,
    ProjectOverviewComponent,
    ProjectMilestoneComponent,
    ProjectTimelineComponent
  ],
  imports: [
    SharedModule,
    ProjectRouting,
    TranslateModule
  ]
})
export class ProjectModule {
}
