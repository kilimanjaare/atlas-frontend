import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProjectOverviewComponent } from './pages/project-overview/project-overview.component';
import { ProjectTimelineComponent } from './pages/project-timeline/project-timeline.component';
import { ProjectIndexComponent } from './pages/project-index/project-index.component';
import { ProjectViewComponent } from './pages/project-view/project-view.component';
import { ProjectMilestoneComponent } from './pages/project-milestone/project-milestone.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectIndexComponent
  },
  {
    path: ':id',
    component: ProjectViewComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        component: ProjectOverviewComponent
      },
      {
        path: 'plan-budget-goals',
        component: ProjectMilestoneComponent
      },
      {
        path: 'timeline',
        component: ProjectTimelineComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRouting {
}
