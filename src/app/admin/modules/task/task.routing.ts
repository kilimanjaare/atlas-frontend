import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { TaskIndexComponent } from './pages/task-index/task-index.component';

const routes: Routes = [
  {
    path: '',
    component: TaskIndexComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRouting {
}
