import { NgModule } from '@angular/core';
import { TaskRouting } from './task.routing';
import { TaskIndexComponent } from './pages/task-index/task-index.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    TaskIndexComponent
  ],
  imports: [
    SharedModule,
    TaskRouting
  ]
})
export class TaskModule {
}
