import { NgModule } from '@angular/core';

import { DefaultDashboardComponent } from './default/default.component';
import { DashboardRoutingModule } from './dashboards.routing';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [DefaultDashboardComponent],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule {
}
