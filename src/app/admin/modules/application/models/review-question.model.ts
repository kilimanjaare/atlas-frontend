export interface ReviewQuestion {
  id: number;
  question: string;
}
