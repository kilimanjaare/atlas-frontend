import { Plan } from '@root/src/app/admin/modules/plan/models/plan.model';
import { Organization } from '@core/models/organization.model';
import { Interest } from '@root/src/app/admin/modules/interest/models/interest.model';
import { GeographicalArea } from '@root/src/app/admin/modules/geographical-area/models/geographical-area.model';
import { ReviewRating } from '@root/src/app/admin/modules/application/models/review-rating.model';

export interface Application {
  id: number;
  amount: number;
  submitted_at: string;
  revisit_at: string;
  description: string;
  is_favorite: boolean;
  review_rating: number;
  approved: number;
  plan: Plan;
  organization: Organization;
  interest: Interest;
  geographicalArea: GeographicalArea;
  reviewRatings: ReviewRating[];
}
