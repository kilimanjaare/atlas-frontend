import { ReviewQuestion } from '@root/src/app/admin/modules/application/models/review-question.model';
import { Application } from '@root/src/app/admin/modules/application/models/application.model';

export interface ReviewRating {
  id: number;
  review_question_id: number;
  application_id: number;
  rating: number;
  reviewQuestion: ReviewQuestion;
  application: Application;
}
