import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationApiService } from '@admin-modules/application/services/application-api.service';
import { Application } from '@admin-modules/application/models/application.model';
import { first } from '@node_modules/rxjs/internal/operators';
import { ToastrService } from '@node_modules/ngx-toastr';

@Component({
  selector: 'app-application-editor',
  templateUrl: 'application-editor.component.html'
})
export class ApplicationEditorComponent implements OnInit {
  public application: Application;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private applicationApiService: ApplicationApiService,
    private toastrService: ToastrService,
  ) {
  }

  public ngOnInit(): void {
    this.loadApplication();
  }

  public submit(data: any): void {
    this.applicationApiService.update(this.getId(), data).pipe(first()).subscribe(response => {
      this.application = response;
      this.toastrService.success('Application was successfully updated');
    });
  }

  private loadApplication(): void {
    this.applicationApiService.show(this.getId()).pipe(first()).subscribe(response => {
      this.application = response;
    });
  }

  private getId(): number {
    return this.route.snapshot.params.id;
  }
}
