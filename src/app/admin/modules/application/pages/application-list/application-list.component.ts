import { Component, OnInit } from '@angular/core';
import { ApplicationApiService } from '@admin-modules/application/services/application-api.service';
import { Application } from '@admin-modules/application/models/application.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionRoutes, BasicRoutes } from '@core/enums/routes';
import { FoundationApiService } from '@admin-modules/foundation/services/foundation-api.service';
import { UserService } from '@admin-modules/user/services/user.service';
import { MetaApi } from '@core/models/meta.api.model';
import { first } from '@node_modules/rxjs/internal/operators';
import { ToastrService } from '@node_modules/ngx-toastr';

@Component({
  selector: 'app-application-list',
  templateUrl: 'application-list.component.html'
})
export class ApplicationListComponent implements OnInit {
  public applications: Application[] = [];
  public meta: MetaApi;
  public page = 1;
  public size = 10;
  public type = '';

  constructor(
    private applicationApiService: ApplicationApiService,
    private foundationApiService: FoundationApiService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private toastrService: ToastrService,
  ) {
  }

  public ngOnInit(): void {
    if (this.route.snapshot.params.type) {
      this.route.params.subscribe(params => {
        this.type = params.type;
        this.loadData();
      });
    } else {
      this.loadData();
    }
  }

  public onEdit(id: number): void {
    this.router.navigate([BasicRoutes.admin, BasicRoutes.applications, id, ActionRoutes.edit]);
  }

  public onView(id: number): void {
    this.router.navigate([BasicRoutes.admin, BasicRoutes.applications, id]);
  }

  public onDestroy(id: number): void {
    if (confirm('Do you want to delete the application?')) {
      this.applicationApiService.destroy(id).pipe(first()).subscribe(response => {
        this.toastrService.success('Application was successfully deleted!');
        this.loadData();
      });
    }
  }

  public loadData(): void {
    const include = ['plan.currency', 'organization'];

    let filters = [];
    if (this.type) {
      filters = [{ field: 'type', value: this.type }];
    }

    this.foundationApiService.applications(this.userService.current().foundation_id, {
      include,
      filters,
      page: this.page,
      perPage: this.size
    }).subscribe(({ data, meta }) => {
      this.applications = data;
      this.meta = meta;
    });
  }
}
