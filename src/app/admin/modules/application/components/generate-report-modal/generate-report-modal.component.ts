import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgbModal } from '@node_modules/@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-generate-report-modal',
  templateUrl: 'generate-report-modal.component.html'
})
export class GenerateReportModalComponent implements OnChanges {
  @Input() public show = false;
  @Output() public close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('content', { static: true }) public content: ElementRef;

  public form: FormGroup;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
    this.createForm();
  }

  public ngOnChanges({ show }: SimpleChanges): void {
    if (show && show.currentValue) {
      this.showModal();
    }
  }

  public showModal(): void {
    this.modalService.open(this.content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.close.emit(true);
    }, (reason) => {
      this.close.emit(true);
    });
  }

  public setRating(rating, input): void {
    this.form.get(input).patchValue(rating.newValue);
  }

  private createForm(): void {
    this.form = this.fb.group({
      by_submission_date: false,
      submission_date_min: null,
      submission_date_max: null,
      by_rating: false,
      rating_min: [1],
      rating_max: [1],
      by_amount: false,
      amount_min: null,
      amount_max: null,
    });
  }
}
