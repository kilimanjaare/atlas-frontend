import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ApplicationApiService } from '@root/src/app/admin/modules/application/services/application-api.service';
import { ActivatedRoute } from '@angular/router';
import { Application } from '@root/src/app/admin/modules/application/models/application.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from '@node_modules/rxjs';
import { InterestApiService } from '@root/src/app/admin/modules/interest/services/interest-api.service';
import { List } from '@core/models/api/list';
import { GeographicalAreaApiService } from '@root/src/app/admin/modules/geographical-area/services/geographical-area-api.service';

@Component({
  selector: 'app-application-form',
  templateUrl: 'application-form.component.html'
})
export class ApplicationFormComponent implements OnInit, OnChanges {
  @Input() public application: Application;
  @Output() public submitted: EventEmitter<any> = new EventEmitter<any>();
  public form: FormGroup;
  public interests$: Observable<List[]>;
  public geographicalAreas$: Observable<List[]>;

  constructor(
    private applicationApiService: ApplicationApiService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private interestApiService: InterestApiService,
    private geographicalAreaApiService: GeographicalAreaApiService,
  ) {
  }

  public ngOnInit(): void {
    this.createForm();
    this.initOptions();
  }

  public ngOnChanges({ application }: SimpleChanges): void {
    if (application && application.currentValue) {
      this.form.patchValue(application.currentValue);
    }
  }

  public submit(): void {
    this.submitted.emit(this.form.value);
  }

  private createForm(): void {
    this.form = this.fb.group({
      description: '',
      amount: '',
      interest_id: null,
      geographical_area_id: null,
    });
  }

  private initOptions(): void {
    this.interests$ = this.interestApiService.list();
    this.geographicalAreas$ = this.geographicalAreaApiService.list();
  }
}
