import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgbModal } from '@node_modules/@ng-bootstrap/ng-bootstrap';
import { ApplicationApiService } from '@admin-modules/application/services/application-api.service';
import { Application } from '@admin-modules/application/models/application.model';

@Component({
  selector: 'app-approve-modal',
  templateUrl: 'approve-modal.component.html'
})
export class ApproveModalComponent implements OnChanges {
  @Input() public show = false;
  @Input() public application: Application;
  @Output() public close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('content', { static: true }) public content: ElementRef;

  constructor(
    private modalService: NgbModal,
    private applicationApiService: ApplicationApiService,
  ) {
  }

  public ngOnChanges({ show }: SimpleChanges): void {
    if (show && show.currentValue) {
      this.showModal();
    }
  }

  public showModal(): void {
    this.modalService.open(this.content).result.then((result) => {
      this.close.emit(true);
    }, (reason) => {
      this.close.emit(true);
    });
  }

  public approve(): void {
    this.applicationApiService.approve(this.application.id).subscribe(response => {
    });
  }
}

