import {
  Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild
} from '@angular/core';
import { NgbModal } from '@node_modules/@ng-bootstrap/ng-bootstrap';
import { ReviewQuestion } from '@admin-modules/application/models/review-question.model';
import { ReviewRating } from '@admin-modules/application/models/review-rating.model';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-feedback-modal',
  templateUrl: 'feedback-modal.component.html'
})
export class FeedbackModalComponent implements OnInit, OnChanges {
  @Input() public show = false;
  @Input() public questions: ReviewQuestion[] = [];
  @Input() public ratings: ReviewRating[] = [];
  @Output() public close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() public submitted: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('content', { static: true }) public content: ElementRef;

  public formRating: FormArray;
  public form: FormGroup;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      formRating: this.fb.array([]),
    });
  }

  public ngOnChanges({ show }: SimpleChanges): void {
    if (show && show.currentValue) {
      this.showModal();
      this.fillForm();
    }
  }

  public showModal(): void {
    this.modalService.open(this.content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.close.emit(true);
    }, (reason) => {
      this.close.emit(true);
    });
  }

  public rate(rate, index): void {
    this.formRating = this.form.get('formRating') as FormArray;

    this.formRating.controls[index].get('rating').patchValue(rate.newValue);
  }

  public submit(): void {
    this.submitted.emit(this.form.value);
  }

  private fillForm(): void {
    this.formRating = this.form.get('formRating') as FormArray;
    this.formRating.clear();

    this.questions.forEach(question => {
      const rating = this.ratings.find(item => item.review_question_id === question.id);

      this.addQuestion(question.question, question.id, rating ? rating.rating : 1);
    });
  }

  private addQuestion(question: string, questionId: number, rating: number): void {
    this.formRating = this.form.get('formRating') as FormArray;

    this.formRating.push(this.fb.group({ question, question_id: questionId, rating }));
  }
}
