import { Component, OnInit } from '@angular/core';
import { ApplicationApiService } from '@root/src/app/admin/modules/application/services/application-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Application } from '@root/src/app/admin/modules/application/models/application.model';
import { first } from '@node_modules/rxjs/internal/operators';
import { AllRoutes } from '@core/enums/routes';
import { ToastrService } from '@node_modules/ngx-toastr';
import { FoundationApiService } from '@admin-modules/foundation/services/foundation-api.service';
import { CookieService } from '@core/services/cookie.service';
import { User } from '@admin-modules/user/models/user.model';
import { ReviewQuestion } from '@admin-modules/application/models/review-question.model';
import { ReviewRating } from '@admin-modules/application/models/review-rating.model';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-application-view',
  templateUrl: 'application-view.component.html'
})
export class ApplicationViewComponent implements OnInit {
  public application: Application;
  public applicationLoaded = false;
  public routes = AllRoutes;
  public showFeedback = false;
  public showApprove = false;
  public reviewQuestions: ReviewQuestion[] = [];
  public reviewRatings: ReviewRating[] = [];
  private authUser: User;

  constructor(
    private applicationApiService: ApplicationApiService,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private foundationApiService: FoundationApiService,
    private cookieService: CookieService,
    private translateService: TranslateService,
  ) {
    this.loadAuthUser();
  }

  public ngOnInit(): void {
    this.loadApplication();
    this.loadReviews();
  }

  public toggleFavorite(): void {
    if (this.application.is_favorite) {
      this.applicationApiService.unsetFavorite(this.application.id).pipe(first()).subscribe(response => {
        this.loadApplication();
      });
    } else {
      this.applicationApiService.setFavorite(this.application.id).pipe(first()).subscribe(response => {
        this.loadApplication();
      });
    }
  }

  public destroy(): void {
    if (confirm('Do you want to delete the application?')) {
      this.applicationApiService.destroy(this.application.id).subscribe(response => {
        this.toastrService.success(this.translateService.instant('Application was successfully deleted'));
        this.router.navigate(['../'], { relativeTo: this.route });
      });
    }
  }

  public submit(data): void {
    this.applicationApiService.updateReviewRating(this.getId(), data).pipe(first()).subscribe(response => {
      this.loadApplication();
      this.loadReviews();
    });
  }

  public reject(): void {
    this.applicationApiService.decline(this.getId()).subscribe(response => {
      this.loadApplication();
    });
  }

  public loadApplication(): void {
    this.applicationLoaded = false;

    const include = ['plan.currency', 'organization', 'interest', 'geographicalArea', 'formAnswer', 'reviewRatings'];

    this.applicationApiService.show(this.getId(), { include })
      .pipe(first())
      .subscribe(response => {
        this.application = response;
        this.applicationLoaded = true;
      });
  }

  private loadReviews(): void {
    this.foundationApiService.reviewQuestions(this.authUser.foundation_id).subscribe(response => {
      this.reviewQuestions = response;
    });

    this.applicationApiService.getReviewRatings(this.getId()).subscribe(response => {
      this.reviewRatings = response;
    });
  }

  private loadAuthUser(): void {
    this.authUser = JSON.parse(this.cookieService.getCookie('currentUser'));
  }

  private getId(): number {
    return this.route.snapshot.params.id;
  }
}
