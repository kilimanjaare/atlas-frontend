import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Application } from '@admin-modules/application/models/application.model';

@Component({
  selector: 'app-application-table',
  templateUrl: 'application-table.component.html'
})
export class ApplicationTableComponent {
  @Input() public applications: Application[] = [];
  @Input() public size = 10;
  @Input() public page = 1;
  @Input() public total = 0;
  @Output() public pageChange: EventEmitter<number> = new EventEmitter();
  @Output() public sizeChange: EventEmitter<number> = new EventEmitter();
  @Output() public edit: EventEmitter<number> = new EventEmitter<number>();
  @Output() public view: EventEmitter<number> = new EventEmitter<number>();
  @Output() public destroy: EventEmitter<number> = new EventEmitter<number>();

  public showGenerateReport = false;
}
