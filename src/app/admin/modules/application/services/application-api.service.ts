import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable } from '@node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationApiService extends ApiService {
  public baseUrl = 'applications';

  public setFavorite(id: number): Observable<any> {
    return this.put(`${id}/favorite`, {});
  }

  public unsetFavorite(id: number): Observable<any> {
    return this.delete(`${id}/favorite`);
  }

  public getReviewRatings(id: number): Observable<any> {
    return this.get(`${id}/review-ratings`);
  }

  public updateReviewRating(id: number, data: any): Observable<any> {
    return this.post(`${id}/review-ratings`, data);
  }

  public approve(id: number, data = {}): Observable<any> {
    return this.put(`${id}/approve`, data);
  }

  public decline(id: number, data = {}): Observable<any> {
    return this.put(`${id}/decline`, data);
  }
}
