import { NgModule } from '@angular/core';
import { ApplicationRouting } from './application.routing';
import { CommonModule } from '@angular/common';
import { RatingModule } from '@node_modules/ng-starrating';
import { ApplicationViewComponent } from '@root/src/app/admin/modules/application/components/application-view/application-view.component';
import { SharedModule } from '@shared/shared.module';
import { ApplicationFormComponent } from '@root/src/app/admin/modules/application/components/application-form/application-form.component';
import { FeedbackModalComponent } from '@root/src/app/admin/modules/application/components/feedback-modal/feedback-modal.component';
import { NgbDropdownModule } from '@node_modules/@ng-bootstrap/ng-bootstrap';
import { ApproveModalComponent } from '@root/src/app/admin/modules/application/components/approve-modal/approve-modal.component';
import { ApplicationTableComponent } from '@admin-modules/application/components/application-table/application-table.component';
import { ApplicationListComponent } from '@admin-modules/application/pages/application-list/application-list.component';
import { ApplicationEditorComponent } from '@admin-modules/application/pages/application-editor/application-editor.component';
import { ApplicationViewerComponent } from '@admin-modules/application/pages/application-viewer/application-viewer.component';
import { TranslateModule } from '@node_modules/@ngx-translate/core';
import { GenerateReportModalComponent } from '@admin-modules/application/components/generate-report-modal/generate-report-modal.component';

@NgModule({
  declarations: [
    ApplicationListComponent,
    ApplicationViewComponent,
    ApplicationFormComponent,
    FeedbackModalComponent,
    ApproveModalComponent,
    ApplicationTableComponent,
    ApplicationEditorComponent,
    ApplicationViewerComponent,
    GenerateReportModalComponent,
  ],
  imports: [
    ApplicationRouting,
    CommonModule,
    RatingModule,
    SharedModule,
    NgbDropdownModule,
    TranslateModule,
  ],
  exports: [
    ApplicationTableComponent,
    ApplicationFormComponent,
    ApplicationViewComponent,
    GenerateReportModalComponent,
  ]
})
export class ApplicationModule {
}
