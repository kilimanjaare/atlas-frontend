import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ActionRoutes } from '@core/enums/routes';
import { ApplicationListComponent } from '@admin-modules/application/pages/application-list/application-list.component';
import { ApplicationEditorComponent } from '@admin-modules/application/pages/application-editor/application-editor.component';
import { ApplicationViewerComponent } from '@admin-modules/application/pages/application-viewer/application-viewer.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationListComponent,
  },
  {
    path: 'type/:type',
    component: ApplicationListComponent,
  },
  {
    path: `:id/${ActionRoutes.edit}`,
    component: ApplicationEditorComponent,
  },
  {
    path: `:id`,
    component: ApplicationViewerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRouting {
}
