import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable } from '@node_modules/rxjs';
import { MetaParams } from '@core/models/api/metaParams';

@Injectable({
  providedIn: 'root'
})
export class FoundationApiService extends ApiService {
  public baseUrl = 'foundations';

  public team(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/team`, params);
  }

  public reviewQuestions(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/review-questions`, params);
  }

  public getPlans(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/plans`, params);
  }

  public applications(id: number, params: MetaParams = {}): Observable<any> {
    return this.get(`${id}/applications`, params);
  }
}
