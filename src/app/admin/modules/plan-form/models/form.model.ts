import { FormElement } from './form-element.model';

export interface Form {
  description: string;
  end_edit_at: string;
  end_submission_at: string;
  id: number;
  name: string;
  elements?: FormElement[];
}
