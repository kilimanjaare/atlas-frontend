import { ElementTypes } from '../enums/element-types.enum';

export interface FormElement {
  id?: number;
  type: ElementTypes;
  label: string;
  is_required: boolean;
  weight: number;
  options?: FormElementOption[];
  rows?: FormElementTableOption[];
  columns?: FormElementTableOption[];
}

export interface FormElementOption {
  id?: number;
  name: string;
  is_default?: boolean;
}

export interface FormElementTableOption {
  id?: number;
  name: string;
}
