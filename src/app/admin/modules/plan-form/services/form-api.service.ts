import { Injectable } from '@angular/core';
import { BaseApiService } from '@core/services/base.api.service';

@Injectable({
  providedIn: 'root'
})
export class FormApiService extends BaseApiService {
  protected baseUrl = '/forms';
}
