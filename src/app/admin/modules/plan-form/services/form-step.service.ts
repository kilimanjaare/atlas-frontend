import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { FormElement } from '../models/form-element.model';
import { Form } from '../models/form.model';

@Injectable({
  providedIn: 'root'
})
export class FormStepService {
  private step$ = new Subject<number>();
  private stepStatus$ = new Subject<{ step: number, status: boolean, data?: any }>();
  private form$ = new Subject<Form>();

  next(step: number) {
    this.step$.next(step);
  }

  stepChanges() {
    return this.step$.asObservable();
  }

  setStepStatus(status: { step: number, status: boolean, data?: any }) {
    this.stepStatus$.next(status);
  }

  stepStatusChanges() {
    return this.stepStatus$.asObservable();
  }

  setForm(form: Form) {
    this.form$.next(form);
  }

  formChanges() {
    return this.form$.asObservable();
  }
}
