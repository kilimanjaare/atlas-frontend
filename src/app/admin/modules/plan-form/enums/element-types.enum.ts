export enum ElementTypes {
  text = 'text',
  textarea = 'textarea',
  note = 'note',
  dropdown = 'dropdown',
  checkbox = 'checkbox',
  grid = 'grid',
  file = 'file'
}
