import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormStepOneComponent } from './pages/form-step-one/form-step-one.component';
import { FormStepTwoComponent } from './pages/form-step-two/form-step-two.component';
import { FormStepThreeComponent } from './pages/form-step-three/form-step-three.component';
import { FormIndexComponent } from './pages/form-index/form-index.component';
import { FormDetailsComponent } from './pages/form-details/form-details.component';

const routes: Routes = [
  {
    path: '',
    component: FormIndexComponent
  },
  {
    path: ':id',
    component: FormDetailsComponent,
    children: [
      {
        path: '',
        redirectTo: 'step-1',
        pathMatch: 'full'
      },
      {
        path: 'step-1',
        component: FormStepOneComponent
      },
      {
        path: 'step-2',
        component: FormStepTwoComponent
      },
      {
        path: 'step-3',
        component: FormStepThreeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanFormRouting {
}
