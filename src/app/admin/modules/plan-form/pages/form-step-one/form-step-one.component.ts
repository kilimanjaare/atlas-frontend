import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormStepService } from '../../services/form-step.service';
import { SubSink } from 'subsink';
import { Form } from '../../models/form.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form-step-one',
  templateUrl: './form-step-one.component.html'
})
export class FormStepOneComponent implements OnInit, OnDestroy {
  form: FormGroup;
  submitted = false;
  formData$: Observable<Form>;

  private subSink = new SubSink();

  constructor(private formBuilder: FormBuilder,
              private formStepService: FormStepService) {
  }

  ngOnInit(): void {
    this.buildForm();

    this.subSink.add(
      this.formData$.subscribe(value => {
        this.form.patchValue(value || {});
      }),
      this.formStepService.stepChanges().subscribe(step => {
        const status = step === 1;

        if (this.form.valid) {
          this.formStepService.setStepStatus({ step: Math.abs(step), status, data: this.form.value });
        } else {
          this.submitted = true;
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id: [null],
      name: [null, Validators.required],
      description: [null, Validators.required],
      end_submission_at: [null],
      end_edit_at: [null]
    });
  }
}
