import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { FormStepService } from '../../services/form-step.service';
import { Form } from '../../models/form.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form-step-three',
  templateUrl: './form-step-three.component.html'
})
export class FormStepThreeComponent implements OnInit, OnDestroy {
  formData$: Observable<Form>;

  private subSink = new SubSink();

  constructor(private formStepService: FormStepService) {
  }

  ngOnInit(): void {
    this.subSink.sink = this.formStepService.stepChanges().subscribe(step => {
      const status = step === 3;
      this.formStepService.setStepStatus({ step: Math.abs(step), status });
    });
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
