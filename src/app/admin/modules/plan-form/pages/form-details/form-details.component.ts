import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormStepService } from '../../services/form-step.service';
import { SubSink } from 'subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { FormApiService } from '../../services/form-api.service';
import { Form } from '../../models/form.model';
import { Observable, of } from 'rxjs';
import { flatMap, map, shareReplay } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-component-plan-form-create',
  templateUrl: './form-details.component.html'
})
export class FormDetailsComponent implements OnInit, OnDestroy {
  activeStep = 1;
  isViewMode = false;

  private steps = [1, 2, 3];
  private formData$: Observable<Form>;
  private subSink = new SubSink();
  private formId: string;

  constructor(
    private formStepService: FormStepService,
    private formApi: FormApiService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.isViewMode = this.route.snapshot.queryParamMap.get('action') === 'view';
    this.activeStep = +this.route.firstChild.snapshot.routeConfig.path.slice(-1);

    if (this.activeStep !== 1 && this.formId === 'new') {
      this.router.navigate(['.', 'step-1'], { relativeTo: this.route });
    }

    this.subSink.sink = this.formStepService.stepStatusChanges().subscribe(value => this.moveToNextStep(value));
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  private moveToNextStep(stepDetails) {
    const nextStepIndex = stepDetails.status ? 1 : -1;
    const activeStepIndex = this.steps.indexOf(stepDetails.step);
    const nextActiveStep = this.steps[activeStepIndex + nextStepIndex];

    this.setFormDetails(stepDetails.step, stepDetails.data);

    if (nextActiveStep) {
      this.activeStep = nextActiveStep;

      const nextStep = this.route.firstChild.snapshot.routeConfig.path.slice(0, -1) + this.activeStep;

      this.router.navigate(['.', nextStep], { relativeTo: this.route });
    } else if (activeStepIndex === this.steps.length - 1) {
      this.subSink.sink = this.formData$.pipe(
        flatMap(data => !data.id ? this.formApi.create(data) : this.formApi.update(data.id, data))
      ).subscribe(response => {
        this.toastr.success(this.translateService.instant('Form was successfully saved'));
        this.router.navigate(['../'], { relativeTo: this.route });
      });
    } else if (activeStepIndex === 0) {
      this.router.navigate(['../'], { relativeTo: this.route });
    }
  }

  setForm(component: any) {
    if (!this.formData$) {
      this.fetchForm();
    }
    component.formData$ = this.formData$;
  }

  private setFormDetails(step, data) {
    switch (step) {
      case 1: {
        this.formData$ = this.formData$.pipe(
          map(result => {
            return { ...result, ...data };
          })
        );
        break;
      }
      case 2: {
        this.formData$ = this.formData$.pipe(
          map(result => {
            return { ...result, elements: data || [] };
          })
        );
      }
    }
  }

  private fetchForm() {
    if (!this.formId) {
      this.setFormId();
    }

    if (this.formId !== 'new') {
      const include = 'elements, elements.type, elements.options, elements.rows, elements.columns';

      this.formData$ = this.formApi.show(this.formId, { include }).pipe(shareReplay(1));
    } else {
      this.formData$ = of(null);
    }
  }

  private setFormId() {
    this.formId = this.route.snapshot.paramMap.get('id');
  }
}
