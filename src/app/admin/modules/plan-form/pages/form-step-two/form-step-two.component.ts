import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { FormStepService } from '../../services/form-step.service';
import { FormElement } from '../../models/form-element.model';
import { ToastrService } from 'ngx-toastr';
import { FormApiService } from '../../services/form-api.service';
import { Form } from '../../models/form.model';
import { Observable } from 'rxjs';
import { ElementTypes } from '../../enums/element-types.enum';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-form-step-two',
  templateUrl: './form-step-two.component.html',
  styleUrls: ['./form-step-two.component.scss']
})
export class FormStepTwoComponent implements OnInit, OnDestroy {
  elementTypes = ElementTypes;
  dropdownItems: { type: ElementTypes, label: string, icon: string }[] = [
    {
      type: this.elementTypes.text,
      label: this.translateService.instant('Text Field'),
      icon: 'mdi-text-short'
    },
    {
      type: this.elementTypes.textarea,
      label: this.translateService.instant('Textarea'),
      icon: 'mdi-text-subject'
    },
    {
      type: this.elementTypes.file,
      label: this.translateService.instant('File/Image Upload'),
      icon: 'mdi-upload'
    },
    {
      type: this.elementTypes.dropdown,
      label: this.translateService.instant('Dropdown List'),
      icon: 'mdi-arrow-down-drop-circle'
    },
    {
      type: this.elementTypes.checkbox,
      label: this.translateService.instant('Checkbox'),
      icon: 'mdi-checkbox-marked'
    },
    {
      type: this.elementTypes.note,
      label: this.translateService.instant('Note'),
      icon: 'mdi-note'
    },
    {
      type: this.elementTypes.grid,
      label: this.translateService.instant('Table Grid'),
      icon: 'mdi-table'
    }
  ];
  optionModel: string;
  checkboxOptionModel: string;
  tableRowOptionModel: string;
  tableColumnOptionModel: string;
  formData$: Observable<Form>;
  formElements: FormElement[] = [];
  private subSink = new SubSink();

  constructor(private formStepService: FormStepService,
              private toastrService: ToastrService,
              private translateService: TranslateService,
              private formApi: FormApiService) {
  }

  ngOnInit(): void {
    this.subSink.add(
      this.formStepService.stepChanges().subscribe(step => {
        if (step === 2) {
          if (!this.formElements.length) {
            this.toastrService.error(this.translateService.instant('Please add elements to the form'));
          } else {
            this.formStepService.setStepStatus({ step: Math.abs(step), status: true, data: this.formElements });
          }
        } else {
          this.formStepService.setStepStatus({ step: Math.abs(step), status: false, data: this.formElements });
        }
      }),
      this.formData$.subscribe((value: any) => {
        if (value && value.elements) {
          this.formElements = value.elements;
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

  addElement({ type }) {
    this.formElements.push({ type, label: '', is_required: false, weight: 0 });
  }

  addOptionForElement(element: FormElement, value: string) {
    const option = { name: value, is_default: undefined };

    if (element.options) {
      element.options.push(option);
    } else {
      element.options = [option];
    }

    this.optionModel = null;
    this.checkboxOptionModel = null;
  }

  setDefaultOption(element: FormElement, optionIndex: number) {
    element.options.forEach((option, index) => {
      option.is_default = optionIndex === index;
    });
  }

  addOptionForTableElement(element: FormElement, type: string, name?) {
    if (element[type] && element[type].length) {
      if (!element[type].find(item => item.name === name)) {
        element[type].push({ name });
      }
    } else {
      element[type] = [{ name }];
    }
  }

  removeElementById(elements, index: number) {
    elements.splice(index, 1);
  }
}
