import { Component, OnInit } from '@angular/core';
import { FormApiService } from '../../services/form-api.service';
import { Observable } from 'rxjs';
import { Form } from '../../models/form.model';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-component-plan-forms',
  templateUrl: './form-index.component.html'
})
export class FormIndexComponent implements OnInit {
  forms$: Observable<Form[]>;

  constructor(
    private formApi: FormApiService,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.fetchForms();
  }

  delete(id: number) {
    this.formApi.destroy(id).subscribe(() => {
      this.toastr.info(this.translateService.instant('Form was deleted'));
      this.fetchForms();
    });
  }

  private fetchForms() {
    this.forms$ = this.formApi.index<Form[]>().pipe(map(response => response.data));
  }
}
