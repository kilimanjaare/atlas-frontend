import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-option-field',
  templateUrl: './add-option-field.component.html'
})
export class AddOptionFieldComponent {
  @Output() add = new EventEmitter<string>();
}
