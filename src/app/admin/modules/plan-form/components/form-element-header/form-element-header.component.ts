import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormElement } from '../../models/form-element.model';

@Component({
  selector: 'app-form-element-header',
  templateUrl: './form-element-header.component.html',
  styleUrls: ['./form-element-header.component.scss']
})
export class FormElementHeaderComponent {
  @Input() element: FormElement;
  @Input() index: number;
  @Output() remove = new EventEmitter<number>();
}
