import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormStepService } from '../../services/form-step.service';
import { TranslateService } from '@node_modules/@ngx-translate/core';

@Component({
  selector: 'app-form-step-footer',
  templateUrl: './form-step-footer.component.html'
})
export class FormStepFooterComponent implements OnInit, OnChanges {
  @Input() activeStep: number;

  labels = {
    previous: '',
    next: ''
  };

  constructor(
    public formStepService: FormStepService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.activeStep && changes.activeStep.currentValue) {
      switch (this.activeStep) {
        case 1: {
          this.labels.previous = this.translateService.instant('Cancel');
          this.labels.next = this.translateService.instant('Next step');
          break;
        }
        case 3: {
          this.labels.previous = this.translateService.instant('Back');
          this.labels.next = this.translateService.instant('Save & share');
          break;
        }
        default: {
          this.labels.previous = this.translateService.instant('Back');
          this.labels.next = this.translateService.instant('Next step');
          break;
        }
      }
    }
  }

  previousStep() {
    this.formStepService.next(-Math.abs(this.activeStep));
  }
}
