import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-form-step-header',
  templateUrl: './form-step-header.component.html',
  styles: [`
    .active {
        font-weight: 700;
    }
  `]
})
export class FormStepHeaderComponent {
  @Input() activeStep: number;
}
