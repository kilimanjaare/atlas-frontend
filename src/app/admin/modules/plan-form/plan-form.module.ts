import { NgModule } from '@angular/core';
import { FormStepOneComponent } from './pages/form-step-one/form-step-one.component';
import { PlanFormRouting } from './plan-form.routing';
import { FormStepThreeComponent } from './pages/form-step-three/form-step-three.component';
import { FormStepTwoComponent } from './pages/form-step-two/form-step-two.component';
import { FormIndexComponent } from './pages/form-index/form-index.component';
import { FormDetailsComponent } from './pages/form-details/form-details.component';
import { SharedModule } from '../../../shared/shared.module';
import { FormStepHeaderComponent } from './components/form-step-header/form-step-header.component';
import { FormStepFooterComponent } from './components/form-step-footer/form-step-footer.component';
import { FormElementHeaderComponent } from './components/form-element-header/form-element-header.component';
import { AddOptionFieldComponent } from './components/add-option-field/add-option-field.component';
import { TranslateModule } from '@node_modules/@ngx-translate/core';

@NgModule({
  declarations: [
    FormIndexComponent,
    FormDetailsComponent,
    FormStepOneComponent,
    FormStepTwoComponent,
    FormStepThreeComponent,
    FormStepHeaderComponent,
    FormStepFooterComponent,
    FormElementHeaderComponent,
    AddOptionFieldComponent
  ],
  imports: [
    SharedModule,
    PlanFormRouting,
    TranslateModule
  ]
})
export class PlanFormModule {
}
