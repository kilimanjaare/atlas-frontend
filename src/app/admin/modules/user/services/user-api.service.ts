import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '@core/services/api.service';

@Injectable({
  providedIn: 'root',
})
export class UserApiService extends ApiService {
  public baseUrl = 'users';

  public current(): Observable<any> {
    return this.get(`current?include=foundation`);
  }
}
