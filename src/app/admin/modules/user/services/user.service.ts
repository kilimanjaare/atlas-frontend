import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { CookieService } from '@core/services/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private cookieService: CookieService,
  ) {
  }

  public setUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUser(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  public current(): User {
    return JSON.parse(this.cookieService.getCookie('currentUser'));
  }
}
