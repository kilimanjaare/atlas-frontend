import { Foundation } from '../../foundation/models/foundation.model';
import { Team } from '../../team/models/team.model';

export interface User {
  id: number;
  name: string;
  position: string;
  email: string;
  phone: string;
  role: string;
  foundation_id: number;
  organization_id: number;
  foundation: Foundation;
  team: Team;
}
