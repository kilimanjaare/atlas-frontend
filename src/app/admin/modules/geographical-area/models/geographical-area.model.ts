export interface GeographicalArea {
  id: number;
  name: string;
}
