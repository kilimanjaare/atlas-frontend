import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class GeographicalAreaApiService extends ApiService {
  public baseUrl = 'geographical-areas';
}
