import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class InterestApiService extends ApiService {
  public baseUrl = 'interests';
}
