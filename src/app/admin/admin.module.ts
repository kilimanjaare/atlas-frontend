import { NgModule } from '@angular/core';
import { AdminRouting } from './admin.routing';
import { AdminOverviewComponent } from './pages/admin-overview/admin-overview.component';
import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgbDropdownModule } from '@node_modules/@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AdminOverviewComponent,
    FooterComponent,
    SidebarComponent,
    TopbarComponent
  ],
  imports: [
    AdminRouting,
    SharedModule,
    TranslateModule,
    NgxPermissionsModule,
    NgbDropdownModule
  ]
})
export class AdminModule {
}
