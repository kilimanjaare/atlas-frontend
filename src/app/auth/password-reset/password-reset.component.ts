import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../core/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './password-reset.component.html',
})
export class PasswordResetComponent implements OnInit {
  public resetForm: FormGroup;
  public submitted = false;
  public error = '';
  public success = '';
  public loading = false;

  public get f() {
    return this.resetForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  public ngOnInit() {
    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  public onSubmit() {
    this.success = '';
    this.submitted = true;

    if (this.resetForm.invalid) {
      return;
    }

    this.loading = true;

    this.authenticationService.reset(this.f.email.value)
      .pipe(first())
      .subscribe(
        response => {
          this.loading = false;
          this.success = 'We have sent you an email containing a link to reset your password';
        },
        error => {
          this.loading = false;
        }
      );
  }
}
