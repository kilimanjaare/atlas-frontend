import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../core/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-auth-password-restore',
  templateUrl: './password-restore.component.html'
})
export class PasswordRestoreComponent implements OnInit {
  public resetForm: FormGroup;
  public loading = false;
  public error = '';
  public submitted = false;

  public get f() {
    return this.resetForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
  }

  public ngOnInit(): void {
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      password_confirmation: ['', Validators.required],
      email: [this.route.snapshot.queryParams.email],
      token: [this.route.snapshot.params.token],
    });
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.resetForm.invalid) {
      return;
    }

    this.loading = true;

    this.authenticationService.restore(this.resetForm.value)
      .pipe(first())
      .subscribe(data => {
          this.loading = false;
          this.router.navigate(['/']);
        },
        error => {
          this.error = error;
          this.loading = false;
        }
      );
  }
}
