import { Injectable } from '@angular/core';
import * as c3 from 'c3';
import { ChartConfiguration } from 'c3';

@Injectable({
  providedIn: 'root',
})
export class ChartService {
  public basicConfig: ChartConfiguration = {
    data: {},
    tooltip: {
      show: false,
    },
    zoom: {
      enabled: false,
    },
    legend: {
      show: false
    },
    point: {
      show: false,
      focus: {
        expand: {
          enabled: false
        }
      }
    }
  };

  public generatePercentageChart(
    elementId: string,
    { percentage, color, height, width }: { percentage: number, color: string, height?: number, width?: number },
  ): any {
    const config: ChartConfiguration = {
      bindto: elementId,
      ...this.basicConfig,
      ...{
        data: {
          type: 'donut',
          order: 'asc',
          columns: [
            ['data1', 100 - percentage],
            ['data2', percentage],
          ]
        },
        color: {
          pattern: ['lightgray', color]
        },
        donut: {
          expand: false,
          width: width ? width : 30,
          title: `${percentage}%`,
          label: {
            show: false
          },
        },
      }
    };

    if (height) {
      config.size = { height };
    }

    return c3.generate(config);
  }

  public generateMonthChart(elementId: string, columns: any[]): any {
    return c3.generate({
      bindto: '#chart-spline',
      data: {
        x: 'x',
        xFormat: '%Y-%m',
        columns,
        types: {
          line: 'area-spline'
        },
        type: 'spline'
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%B'
          }
        }
      },
      legend: {
        show: false,
      }
    });
  }

}
