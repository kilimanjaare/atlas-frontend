import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  public activeLang: BehaviorSubject<string> = new BehaviorSubject<string>('en');
  public activeLang$: Observable<string> = this.activeLang.asObservable();
}
