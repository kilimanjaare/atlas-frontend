import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment.local';
import { Options } from '../models/api/options';
import { MetaParams } from '../models/api/metaParams';
import { Response } from '../models/api/response';
import { Params } from '@angular/router';

export class BaseApiService {
  constructor(
    protected http: HttpClient
  ) {
  }

  protected baseUrl: string;

  public get(url: string, options?: Options): Observable<any> {
    return this.http.get(`${environment.base_url}/api${url}`, options);
  }

  public post(url: string, body: any | null, options?: Options): Observable<any> {
    return this.http.post(`${environment.base_url}/api${url}`, body, options);
  }

  public put(url: string, body: any | null, options?: Options): Observable<any> {
    return this.http.put(`${environment.base_url}/api${url}`, body, options);
  }

  public index<T>(params?: MetaParams): Observable<Response<T>> {
    return this.get(this.baseUrl, params ? this.toMetaParams(params) : {});
  }

  public list<T>(params?: MetaParams): Observable<T> {
    return this.get(`${this.baseUrl}/list`, params ? this.toMetaParams(params) : {});
  }

  public create(payload: any): Observable<any> {
    return this.post(this.baseUrl, payload);
  }

  public update(id: number, payload: any): Observable<any> {
    return this.put(`${this.baseUrl}/${id}`, payload);
  }

  public show(id: string, params?: Params): Observable<any> {
    // TODO: Add the correct response
    return this.get(`${this.baseUrl}/${id}`, { params });
  }

  public destroy(id: number): Observable<any> {
    return this.http.delete(`${environment.base_url}/api${this.baseUrl}/${id}`);
  }

  public toMetaParams(options: MetaParams): any {
    const params = {};

    Object.keys(options).forEach(key => {
      params[`_meta[${key}]`] = options[key];
    });

    return { params };
  }
}
