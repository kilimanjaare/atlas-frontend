import { Injectable } from '@angular/core';
import { BaseApiService } from './base.api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanApiService extends BaseApiService {
  protected baseUrl = '/plans';

  public applications(id: number): Observable<any> {
    return this.get(`${this.baseUrl}/${id}/applications`, { params: { include: 'organization,plan.currency' } });
  }

  public budgetChart(id: number, dateType: string): Observable<any> {
    return this.get(`${this.baseUrl}/${id}/budget/chart`, { params: { date_type: dateType } });
  }
}
