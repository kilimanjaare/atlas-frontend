import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { MetaParams } from '@core/models/api/metaParams';

export class ApiService {
  public baseUrl: string;

  constructor(
    protected http: HttpClient,
  ) {
  }

  public index(params: MetaParams = {}): Observable<any> {
    return this.http.get(`${environment.base_url}/api/${this.baseUrl}${this.getOptionsQuery(params)}`);
  }

  public list(params: MetaParams = {}): Observable<any> {
    return this.http.get(`${environment.base_url}/api/${this.baseUrl}/list${this.getOptionsQuery(params)}`);
  }

  public show(id: number, params: MetaParams = {}): Observable<any> {
    return this.http.get(`${environment.base_url}/api/${this.baseUrl}/${id}${this.getOptionsQuery(params)}`);
  }

  public update(id: number, data: any, params: MetaParams = {}): Observable<any> {
    return this.http.put(`${environment.base_url}/api/${this.baseUrl}/${id}${this.getOptionsQuery(params)}`, data);
  }

  public create(body: any, params: MetaParams = {}): Observable<any> {
    return this.post(this.getOptionsQuery(params), body);
  }

  public destroy(id: number): Observable<any> {
    return this.delete(`${id}`);
  }

  public get(url: string, params: MetaParams = {}): Observable<any> {
    return this.http.get(`${environment.base_url}/api/${this.baseUrl}/${url}${this.getOptionsQuery(params)}`);
  }

  public put(url: string, body: any | null, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<any> {
    return this.http.put(`${environment.base_url}/api/${this.baseUrl}/${url}`, body, options);
  }

  public delete(url: string, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<any> {
    return this.http.delete(`${environment.base_url}/api/${this.baseUrl}/${url}`, options);
  }

  public post(url: string, body: any | null, options?: {
    headers?: HttpHeaders | {
      [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
      [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
  }): Observable<any> {
    return this.http.post(`${environment.base_url}/api/${this.baseUrl}/${url}`, body, options);
  }

  public getOptionsQuery(options) {
    if (!options) {
      return '';
    }

    const parts = [];

    if (options.include) {
      parts.push('include=' + options.include.join(','));
    }

    if (options.page) {
      parts.push('_meta[page]=' + options.page);
    }

    if (options.perPage) {
      parts.push('_meta[perPage]=' + options.perPage);
    }

    if (options.search) {
      options.search.forEach((value, index) => {
        parts.push(`_meta[search][${index}]=${value}`);
      });
    }

    if (options.filters) {
      options.filters.forEach((filter, index) => {
        if (filter.operator !== undefined && filter.operator !== null) {
          parts.push(`_meta[filter][${filter.field}][operator]=${filter.operator}`);
        }
        if (Array.isArray(filter.value)) {
          filter.value.forEach((value, i) => {
            parts.push(`_meta[filter][${filter.field}][value][${i}]=${value}`);
          });
        } else {
          parts.push(`_meta[filter][${filter.field}][value]=${filter.value}`);
        }
      });
    }

    if (options.sort) {
      options.sort.forEach((value, index) => {
        parts.push(`_meta[sort][${index}][${value.field}]=${value.direction}`);
      });
    }

    if (options.has) {
      options.has.forEach((has, index) => {
        if (has.operator !== undefined && has.operator !== null) {
          parts.push(`_meta[has][${has.relation}][operator]=${has.operator}`);
        }
        if (has.count !== undefined && has.count !== null) {
          parts.push(`_meta[has][${has.relation}][count]=${has.count}`);
        }
        if ((has.operator === undefined || has.operator === null) &&
          (has.count === undefined || has.count === null)) {
          parts.push(`_meta[has][${has.relation}]`);
        }
      });
    }
    return '?' + parts.join('&');
  }

  public toHttpParams(params: MetaParams = {}): HttpParams {
    let httpParams = new HttpParams();

    Object.keys(params).forEach(key => {
      if (key === 'include') {
        httpParams = httpParams.append(key, params[key].join(','));
      } else {
        httpParams = httpParams.append(`_meta[${key}]`, params[key]);
      }
    });

    return httpParams;
  }
}
