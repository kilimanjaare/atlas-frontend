import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseApiService } from './base.api.service';

@Injectable({
  providedIn: 'root'
})
export class RoleApiService extends BaseApiService {
  constructor(
    protected http: HttpClient,
  ) {
    super(http);
  }

  public getPermissions(): Observable<any> {
    return this.get('/permissions');
  }
}
