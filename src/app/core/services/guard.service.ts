import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { RoleApiService } from './role-api.service';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GuardService {
  constructor(
    private authService: AuthenticationService,
    private permissionsService: NgxPermissionsService,
    private roleApiService: RoleApiService,
  ) {
  }

  public async canActivate(route, state) {
    if (this.authService.currentUser() && this.authService.currentUser().access_token) {
      await this.roleApiService.getPermissions().pipe(first()).toPromise().then(permissions => {
        this.permissionsService.loadPermissions(permissions);
      });

      return true;
    }

    return false;
  }
}
