import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CookieService } from './cookie.service';
import { User } from '../models/auth.models';
import { BaseApiService } from './base.api.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends BaseApiService {
  user: User;

  constructor(
    protected http: HttpClient,
    private cookieService: CookieService
  ) {
    super(http);
  }

  public currentUser(): User {
    if (!this.user) {
      this.user = JSON.parse(this.cookieService.getCookie('currentUser'));
    }

    return this.user;
  }

  public login(email: string, password: string) {
    return this.post(`/login`, { email, password })
      .pipe(map((user: User) => {
        if (user && user.access_token) {
          this.user = user;
          this.cookieService.setCookie('currentUser', JSON.stringify(user), 1);
        }
        return user;
      }));
  }

  public logout() {
    this.cookieService.deleteCookie('currentUser');
    this.user = null;
  }

  public reset(email: string): Observable<any> {
    return this.post(`/recovery`, { email });
  }

  public restore(data: any): Observable<any> {
    return this.post(`/reset`, data);
  }

  public register(data: any): Observable<any> {
    return this.post(`/register`, data);
  }
}
