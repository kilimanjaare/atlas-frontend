export interface MetaParams {
  include?: string[];
  sort?: {
    field: string;
    direction: 'asc' | 'desc';
  }[];
  perPage?: number;
  page?: number;
  filters?: {
    field: string;
    value: any;
    operator?: string;
  }[];
  search?: string[];
  has?: {
    relation: string;
    count?: number;
    operator?: '=' | '>' | '<' | '>=' | '<=';
  }[];
}
