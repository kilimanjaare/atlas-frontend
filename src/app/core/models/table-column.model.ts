export interface TableColumn {
  field: string;
  label: string;
}
