export interface TableAction {
  icon: string;
  key: string;
}
