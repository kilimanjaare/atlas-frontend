import { Plan } from '../../admin/modules/plan/models/plan.model';
import { Organization } from './organization.model';

export interface Application {
  id: number;
  amount: number;
  submitted_at: string;
  plan: Plan;
  organization: Organization;
}
