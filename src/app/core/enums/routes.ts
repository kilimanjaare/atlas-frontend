export enum BasicRoutes {
  admin = 'admin',
  applications = 'applications',
}

export enum ActionRoutes {
  edit = 'edit',
}

export const AllRoutes = {
  ...ActionRoutes,
};
