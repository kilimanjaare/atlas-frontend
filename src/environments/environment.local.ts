export const environment = {
  production: false,
  hmr: true,
  base_url: 'http://localhost:8000'
};
